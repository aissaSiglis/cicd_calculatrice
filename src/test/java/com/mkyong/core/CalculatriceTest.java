package com.mkyong.core;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.*;


public class CalculatriceTest {
	
	private static long startTime;
    private static long totalDuration;
	
	
    @Test
    public void testAddition() {
        assertEquals(5, Calculatrice.addition(2, 3));
        assertEquals(-1, Calculatrice.addition(2, -3));
        assertEquals(0, Calculatrice.addition(0, 0));
    }

    @Test
    public void testSoustraction() {
        assertEquals(2, Calculatrice.soustraction(5, 3));
        assertEquals(8, Calculatrice.soustraction(5, -3));
        assertEquals(0, Calculatrice.soustraction(0, 0));
    }

    @Test
    public void testMultiplication() {
        assertEquals(6, Calculatrice.multiplication(2, 3));
        assertEquals(-6, Calculatrice.multiplication(2, -3));
        assertEquals(0, Calculatrice.multiplication(0, 0));
    }

    @Test
    public void testDivision() {
        assertEquals(2, Calculatrice.division(6, 3));
        assertEquals(-2, Calculatrice.division(6, -3));
        assertEquals(0, Calculatrice.division(0, 5));
    }
    
    @Test
    public void testDivisionParZero() {
    	assertThrows(ArithmeticException.class, () -> {
        Calculatrice.division(5, 0);
        fail("La division par zéro n'a pas levé d'exception");
    });
    }
    
    
    @Test
    public void testFactoriel() {
        assertEquals(1, Calculatrice.factoriel(0));
        assertEquals(1, Calculatrice.factoriel(1));
        assertEquals(2, Calculatrice.factoriel(2));
        assertEquals(6, Calculatrice.factoriel(3));
        assertEquals(24, Calculatrice.factoriel(4));
    }

   
    @Test
    public void testFactorielNegative() {
    	assertThrows(IllegalArgumentException.class, () -> {
        Calculatrice.factoriel(-1);
        fail("Devrait lancer une exception pour un nombre négatif");
    });
    }

    @Test
    public void testCalculFactoriel() {
        assertEquals(120, Calculatrice.calcul(5, 0, "!")); // 5!
        assertEquals(1, Calculatrice.calcul(0, 0, "!")); // 0!
    }


    @Test
    public void testCalcul() {
        assertEquals(5, Calculatrice.calcul(2, 3, "+"));
        assertEquals(-1, Calculatrice.calcul(2, 3, "-"));
        assertEquals(6, Calculatrice.calcul(2, 3, "*"));
        assertEquals(2, Calculatrice.calcul(6, 3, "/"));
        assertEquals(0, Calculatrice.calcul(2, 3, "x"));
    }
    
    @BeforeAll
    public static void setUpBeforeAllTests() {
        System.out.println("Début de tous les tests pour la classe Calculatrice");
        startTime = System.nanoTime();
    }

    @AfterAll
    public static void tearDownAfterAllTests() {
    	long endTime = System.nanoTime();
        totalDuration = endTime - startTime;
        System.out.println("Fin de tous les tests. Temps total écoulé : " + convertDuration (totalDuration));
        System.out.println("Fin de tous les tests pour la classe Calculatrice");
    }


	@BeforeEach
    public void setUpBeforeEachTest() {
        System.out.println("Début d'un test");
    }

    @AfterEach
    public void tearDownAfterEachTest() {
        System.out.println("Fin d'un test");
    }
    
    private static String convertDuration(long duration) {
        long millis = TimeUnit.NANOSECONDS.toMillis(duration);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        long minutes = TimeUnit.SECONDS.toMinutes(seconds);
        return String.format("%d min, %d sec, %d ms", minutes, seconds % 60, millis % 1000);
    }  
    
    // Test pour assertEquals
    @Test
    public void testAssertEquals() {
        assertEquals(5, Calculatrice.addition(2, 3));
    }

    // Test pour assertNotEquals
    @Test
    public void testAssertNotEquals() {
        assertNotEquals(6, Calculatrice.addition(2, 3));
    }
/*
    // Test pour assertNull
    @Test
    public void testAssertNull() {
        assertNull(Calculatrice.multiplication(5, 0));
    }
*/
    // Test pour assertNotNull
    @Test
    public void testAssertNotNull() {
        assertNotNull(Calculatrice.addition(2, 3));
    }
/*
    // Test pour fail
    @Test
    public void testFail() {
        // On peut appeler fail directement pour forcer un échec de test
    	
        fail("Ceci est un échec de test intentionnel.");
    }
 
    // Test pour vérifier si le nombre est positif
    @Test
    public void testEstPositif() {
        int nombrePositif = 5;
        assertTrue("Le nombre devrait être positif", nombrePositif > 0);
    }

    // Test pour vérifier si le nombre est négatif
    @Test
    public void testEstNegatif() {
        int nombreNegatif = -5;
        assertFalse("Le nombre ne devrait pas être négatif", nombreNegatif > 0);
    }
   */ 
   
}
 