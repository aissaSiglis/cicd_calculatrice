package com.mkyong.core;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;


public class PerroquetTest {

    @Test
    public void testPerroquetParlant() {
        assertEquals("Bonjour", perroquet.perroquetParlant("Bonjour"));
        assertEquals("Hello", perroquet.perroquetParlant("Hello"));
    }

    @Test
    public void testPerroquetBonjour() {
        assertEquals("Bonjour Alice", perroquet.perroquetBonjour("Alice"));
        assertEquals("Bonjour Bob", perroquet.perroquetBonjour("Bob"));
    }
    
    @BeforeAll
    public static void setUpBeforeAllTests() {
        System.out.println("Début de tous les tests pour la classe Perroquet");
    }

    @AfterAll
    public static void tearDownAfterAllTests() {
        System.out.println("Fin de tous les tests pour la classe Perroquet");
    }

    @BeforeEach
    public void setUpBeforeEachTest() {
        System.out.println("Début d'un test");
    }

    @AfterEach
    public void tearDownAfterEachTest() {
        System.out.println("Fin d'un test");
    }

    
    
}