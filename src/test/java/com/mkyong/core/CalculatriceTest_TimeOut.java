package com.mkyong.core;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatriceTest_TimeOut {
	@Test
    @Timeout(30)
    public void testAddition() {
        assertEquals(5, Calculatrice.addition(2, 3));
    }

    @Test
    @Timeout(30)
    public void testSoustraction() {
        assertEquals(2, Calculatrice.soustraction(5, 3));
    }
}
