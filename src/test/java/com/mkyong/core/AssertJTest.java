package com.mkyong.core;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AssertJTest {

    // Test d'addition avec AssertJ
    @Test
    public void testAddition() {
        assertThat(Calculatrice.addition(2, 3)).isEqualTo(5);
    }

    // Test de multiplication avec AssertJ
    @Test
    public void testMultiplication() {
        assertThat(Calculatrice.multiplication(2, 3)).isEqualTo(6);
    }
}