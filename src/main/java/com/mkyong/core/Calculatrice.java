package com.mkyong.core;

public class Calculatrice {

	public static int addition(int a, int b) {
		int add = a+b;
		return add;
	}
	
	public static int soustraction(int a, int b) {
		int sous = a-b;
		return sous;
	}	
	public static int multiplication(int a, int b) {
		int multi = a*b;
		return multi;
	}
	public static int division(int a, int b) {
		if(b==0) {
		throw new ArithmeticException("La division par zéro n'est pas autorisée");	
		}else {
			int divi = a/b;
			return divi;
		}
		}
	
	public static int factoriel(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Le nombre doit être positif ou nul");
        }
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }
	
	public static int calcul(int a, int b, String operateur) {
		switch(operateur) {
		case "+":
			return addition(a,b);
		case "-":
			return soustraction(a,b);
		case "/":
			return division(a,b);
		case "*":
			return multiplication(a,b);
		case "!":
			return factoriel(a);
		default:
			System.out.println("Operateur non pris en charge");
			return 0;
		}
	}	
	
	

}

